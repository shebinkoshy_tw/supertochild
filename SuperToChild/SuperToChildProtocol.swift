//
//  SuperToChild.swift
//  BridgeManager
//
//  Created by Shebin Koshy on 06/05/20.
//  Copyright © 2020 Shebin Koshy. All rights reserved.
//

import UIKit

@objc public protocol SuperToChildProtocol {
    
    @objc var identifier: String? { get set }
    
    @objc func getRootVC() -> UIViewController
    
    @objc func superToChild(action:String, arguments:[String:Any]?, completion:((Any?,Bool) -> Swift.Void)?)
    
}
