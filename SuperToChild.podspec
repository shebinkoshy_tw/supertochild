Pod::Spec.new do |s|
  s.name         = "SuperToChild"
  s.version      = "1.0.2"
  s.summary      = "Sample child app"
  s.description  = "The Sample child app The Sample child app The Sample child app The Sample child app"
  s.homepage     = "https://gitlab.com/shebinkoshy_tw/supertochild.git"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "" => "Shebin Koshy" }
  s.platform     = :ios
  s.source       = { :git => "https://gitlab.com/shebinkoshy_tw/supertochild.git", :tag => '1.0.2' }
  s.ios.deployment_target = "9.0"
  s.source_files = 'SuperToChild/SuperToChildProtocol.swift'
end